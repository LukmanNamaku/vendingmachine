-- phpMyAdmin SQL Dump
-- version 5.2.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Waktu pembuatan: 25 Agu 2023 pada 09.37
-- Versi server: 10.4.28-MariaDB
-- Versi PHP: 8.0.28

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `dbvandingmachine`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `product`
--

CREATE TABLE `product` (
  `productId` int(11) NOT NULL,
  `productName` varchar(255) DEFAULT NULL,
  `productPrice` decimal(19,2) DEFAULT NULL,
  `productQty` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data untuk tabel `product`
--

INSERT INTO `product` (`productId`, `productName`, `productPrice`, `productQty`) VALUES
(1, 'Biskuit', 6000.00, 97),
(2, 'Chips', 8000.00, 100),
(3, 'Oreo', 10000.00, 0),
(4, 'Tango', 10000.00, 100),
(5, 'Tango', 12000.00, 5),
(6, 'Cokelat', 15000.00, 10);

-- --------------------------------------------------------

--
-- Struktur dari tabel `trxmachine`
--

CREATE TABLE `trxmachine` (
  `trxId` int(11) NOT NULL,
  `balance` decimal(19,2) DEFAULT NULL,
  `productId` int(11) DEFAULT NULL,
  `returnedCoins` decimal(19,2) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data untuk tabel `trxmachine`
--

INSERT INTO `trxmachine` (`trxId`, `balance`, `productId`, `returnedCoins`) VALUES
(1, 36100.00, 1, 36100.00),
(2, 100.00, 0, 0.00);

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `product`
--
ALTER TABLE `product`
  ADD PRIMARY KEY (`productId`);

--
-- Indeks untuk tabel `trxmachine`
--
ALTER TABLE `trxmachine`
  ADD PRIMARY KEY (`trxId`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `product`
--
ALTER TABLE `product`
  MODIFY `productId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT untuk tabel `trxmachine`
--
ALTER TABLE `trxmachine`
  MODIFY `trxId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
