package com.test.findingmachine.dto;

import com.test.findingmachine.entity.Product;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

/**
 * @Project findingmachine
 * @Package com.test.findingmachine.dto
 * @Author Lukman Ardi, email : lukman.ardie@gmail.com - PC:ASUS-N56V
 * @Create 25/08/2023
 */

@Data
@AllArgsConstructor
@NoArgsConstructor
public class PurchaseDTO {

    private Integer trxId;
    private Product product;
    private BigDecimal balance;
    private BigDecimal returnedCoins;

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public BigDecimal getBalance() {
        return balance;
    }

    public void setBalance(BigDecimal balance) {
        this.balance = balance;
    }

    public BigDecimal getReturnedCoins() {
        return returnedCoins;
    }

    public void setReturnedCoins(BigDecimal returnedCoins) {
        this.returnedCoins = returnedCoins;
    }

    public Integer getTrxId() {
        return trxId;
    }

    public void setTrxId(Integer trxId) {
        this.trxId = trxId;
    }

}
