package com.test.findingmachine.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

/**
 * @Project findingmachine
 * @Package com.test.findingmachine.dto
 * @Author Lukman Ardi, email : lukman.ardie@gmail.com - PC:ASUS-N56V
 * @Create 25/08/2023
 */

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ProductDTO {

    private Integer productId;
    private String productName;
    private BigDecimal productPrice;

}
