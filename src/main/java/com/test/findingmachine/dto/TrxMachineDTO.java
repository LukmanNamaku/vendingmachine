package com.test.findingmachine.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

/**
 * @Project findingmachine
 * @Package com.test.findingmachine.dto
 * @Author Lukman Ardi, email : lukman.ardie@gmail.com - PC:ASUS-N56V
 * @Create 25/08/2023
 */

@Data
@AllArgsConstructor
@NoArgsConstructor
public class TrxMachineDTO {

    private int productId;
    private BigDecimal balance;
    private BigDecimal returnedCoins;

    public int getProductId() {
        return productId;
    }

    public void setProductId(int productId) {
        this.productId = productId;
    }

    public BigDecimal getBalance() {
        return balance;
    }

    public void setBalance(BigDecimal balance) {
        this.balance = balance;
    }

    public BigDecimal getReturnedCoins() {
        return returnedCoins;
    }

    public void setReturnedCoins(BigDecimal returnedCoins) {
        this.returnedCoins = returnedCoins;
    }
}
