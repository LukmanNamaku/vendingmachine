package com.test.findingmachine.dto;

import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

/**
 * @Project findingmachine
 * @Package com.test.findingmachine.dto
 * @Author Lukman Ardi, email : lukman.ardie@gmail.com - PC:ASUS-N56V
 * @Create 25/08/2023
 */

@Data
public class CreateProductDTO {

    @NotBlank
    @NotNull(message = "product-name-not-null")
    private String productName;
    private BigDecimal productPrice;

}
