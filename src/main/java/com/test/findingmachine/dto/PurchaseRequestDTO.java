package com.test.findingmachine.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @Project findingmachine
 * @Package com.test.findingmachine.dto
 * @Author Lukman Ardi, email : lukman.ardie@gmail.com - PC:ASUS-N56V
 * @Create 25/08/2023
 */

@Data
@AllArgsConstructor
@NoArgsConstructor
public class PurchaseRequestDTO{

    private Integer trxId;
    private Integer productId;

    public Integer getTrxId() {
        return trxId;
    }

    public void setTrxId(Integer trxId) {
        this.trxId = trxId;
    }

    public Integer getProductId() {
        return productId;
    }

    public void setProductId(Integer productId) {
        this.productId = productId;
    }

}
