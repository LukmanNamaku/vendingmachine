package com.test.findingmachine.controller;

import com.test.findingmachine.dto.CreateProductDTO;
import com.test.findingmachine.dto.ProductDTO;
import com.test.findingmachine.dto.UpdateProductDTO;
import com.test.findingmachine.entity.Product;
import com.test.findingmachine.service.ProductService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @Project findingmachine
 * @Package com.test.findingmachine.controller
 * @Author Lukman Ardi, email : lukman.ardie@gmail.com - PC:ASUS-N56V
 * @Create 25/08/2023
 */

@RestController
@RequestMapping("/api/manage-product")
public class ProductController {

    @Autowired
    private ProductService productService;

    @ApiOperation("Get All Data Product")
    @GetMapping
    public ResponseEntity<ProductDTO> getAllProduct() {
        List<Product> newProductList = productService.getAllProduct();
        return new ResponseEntity(newProductList, HttpStatus.OK);
    }

    @ApiOperation("Get Data Product")
    @GetMapping("/{productId}")
    ResponseEntity getOne(@PathVariable Integer productId) {
        Product newProduct = productService.getOneProduk(productId);
        return new ResponseEntity(newProduct, HttpStatus.OK);
    }

    @ApiOperation("Add New Data Product")
    @PostMapping
    ResponseEntity create(@RequestBody CreateProductDTO createProductDTO){
        productService.create(createProductDTO);
        return new ResponseEntity(HttpStatus.CREATED);
    }

    @ApiOperation("Update Data Product")
    @PatchMapping
    ResponseEntity update(@RequestBody UpdateProductDTO updateProductDTO) {
        productService.update(updateProductDTO);
        return new ResponseEntity(HttpStatus.OK);
    }

    @ApiOperation("Delete Data Product")
    @DeleteMapping("/{productId}")
    ResponseEntity delete(@PathVariable Integer productId) {
        productService.delete(productId);
        return new ResponseEntity(HttpStatus.OK);
    }

}
