package com.test.findingmachine.controller;

import com.test.findingmachine.dto.PurchaseDTO;
import com.test.findingmachine.dto.PurchaseRequestDTO;
import com.test.findingmachine.dto.TrxMachineDTO;
import com.test.findingmachine.entity.TrxMachine;
import com.test.findingmachine.service.VendingService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;

/**
 * @Project findingmachine
 * @Package com.test.findingmachine.controller
 * @Author Lukman Ardi, email : lukman.ardie@gmail.com - PC:ASUS-N56V
 * @Create 24/08/2023
 */

@RestController
@RequestMapping("/api/process")
public class VendingController {

    @Autowired
    private VendingService vendingService;

    @ApiOperation("Request Data")
    @GetMapping
    TrxMachineDTO getAccount(@RequestBody TrxMachineDTO accountDTO){
        TrxMachineDTO newAccountDTO = vendingService.getAccount(accountDTO);
        return newAccountDTO;
    }

    @ApiOperation("Add Coin")
    @PostMapping("/addCoin")
    public TrxMachine addCoin(@RequestParam(required=false) Integer trxId, @RequestParam BigDecimal coin){
        TrxMachine newTrxMachine = vendingService.insertCoin(trxId, coin);
        return newTrxMachine;
    }

    @ApiOperation("Purchase Item Product")
    @PostMapping("/purchaseProduct")
    ResponseEntity purchaseProduct(@RequestBody PurchaseRequestDTO purchaseRequestDTO) {
        TrxMachine newTrxMachine = vendingService.purchaseProduct(purchaseRequestDTO);
        PurchaseDTO newPurrcase = vendingService.purchaseResponse(purchaseRequestDTO);
        return new ResponseEntity(newPurrcase, HttpStatus.OK);
    }

    @ApiOperation("Coin Return")
    @PostMapping(value="/coinsReturn")
    ResponseEntity coinsReturn(@RequestBody PurchaseRequestDTO purchaseRequestDTO) {
        TrxMachine newTrxMachine = vendingService.coinReturn(purchaseRequestDTO);
        return new ResponseEntity(newTrxMachine, HttpStatus.OK);
    }

}
