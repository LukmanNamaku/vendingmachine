package com.test.findingmachine.entity;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.math.BigDecimal;

/**
 * @Project findingmachine
 * @Package com.test.findingmachine.entity
 * @Author Lukman Ardi, email : lukman.ardie@gmail.com - PC:ASUS-N56V
 * @Create 25/08/2023
 */

@Data
@Entity
public class TrxMachine {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer trxId;
    private Integer productId;
    private BigDecimal balance;
    private BigDecimal returnedCoins;

    public Integer getTrxId() {
        return trxId;
    }

    public void setTrxId(Integer trxId) {
        this.trxId = trxId;
    }

    public Integer getProductId() {
        return productId;
    }

    public void setProductId(Integer productId) {
        this.productId = productId;
    }

    public BigDecimal getBalance() {
        return balance;
    }

    public void setBalance(BigDecimal balance) {
        this.balance = balance;
    }

    public BigDecimal getReturnedCoins() {
        return returnedCoins;
    }

    public void setReturnedCoins(BigDecimal returnedCoins) {
        this.returnedCoins = returnedCoins;
    }
}
