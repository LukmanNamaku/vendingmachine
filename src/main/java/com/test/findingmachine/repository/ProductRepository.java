package com.test.findingmachine.repository;

import com.test.findingmachine.entity.Product;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

/**
 * @Project findingmachine
 * @Package com.test.findingmachine.repository
 * @Author Lukman Ardi, email : lukman.ardie@gmail.com - PC:ASUS-N56V
 * @Create 25/08/2023
 */


public interface ProductRepository extends JpaRepository<Product, Integer> {

    Optional<Product> findByProductId(Integer productId);
    Product findTopByProductId(Integer productId);

}
