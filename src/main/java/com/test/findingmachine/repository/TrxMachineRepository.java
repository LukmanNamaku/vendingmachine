package com.test.findingmachine.repository;

import com.test.findingmachine.entity.TrxMachine;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

/**
 * @Project findingmachine
 * @Package com.test.findingmachine.repository
 * @Author Lukman Ardi, email : lukman.ardie@gmail.com - PC:ASUS-N56V
 * @Create 25/08/2023
 */


public interface TrxMachineRepository extends JpaRepository<TrxMachine, Integer> {

    Optional<TrxMachine> findByTrxId(Integer trxId);
    TrxMachine findTopByTrxId(Integer trxId);
    TrxMachine findTopByOrderByTrxIdDesc();

}
