package com.test.findingmachine.handler;

/**
 * @Project findingmachine
 * @Package com.test.findingmachine.handler
 * @Author Lukman Ardi, email : lukman.ardie@gmail.com - PC:ASUS-N56V
 * @Create 25/08/2023
 */


public class PembayaranKurangException extends RuntimeException{

    private String message;
    private long remaining;

    public PembayaranKurangException(String message, long remaining) {
        this.message = message;
        this.remaining = remaining;
    }

    public long getRemaining(){
        return remaining;
    }

    @Override
    public String getMessage(){
        return message + remaining;
    }

}
