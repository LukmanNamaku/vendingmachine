package com.test.findingmachine.service.impl;

import com.test.findingmachine.dto.CreateProductDTO;
import com.test.findingmachine.dto.UpdateProductDTO;
import com.test.findingmachine.entity.Product;
import com.test.findingmachine.handler.NotFoundException;
import com.test.findingmachine.repository.ProductRepository;
import com.test.findingmachine.service.ProductService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

/**
 * @Project findingmachine
 * @Package com.test.findingmachine.service.impl
 * @Author Lukman Ardi, email : lukman.ardie@gmail.com - PC:ASUS-N56V
 * @Create 25/08/2023
 */

@Service
public class ProductServiceImpl implements ProductService {

    @Autowired
    private ProductRepository productRepository;

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public List<Product> getAllProduct() {
        List<Product> newProductList = productRepository.findAll();
        return newProductList;
    }

    @Override
    public Product getOneProduk(Integer productId) {
        Product newProduct = productRepository.findTopByProductId(productId);
        if (newProduct == null){
            throw new NotFoundException("product-tidak-ditemukan");
        }
        return newProduct;
    }

    @Override
    public void create(CreateProductDTO createProductDTO) {
        Product newProduct = new Product();
        BeanUtils.copyProperties(createProductDTO, newProduct);
        productRepository.save(newProduct);
    }

    @Override
    public void update(UpdateProductDTO updateProductDTO) {
        Product newProduct = productRepository.findTopByProductId(updateProductDTO.getProductId());
        if (newProduct == null){
            throw new NotFoundException("product-tidak-ditemukan");
        }
        BeanUtils.copyProperties(updateProductDTO, newProduct);
        productRepository.save(newProduct);
    }

    @Override
    public void delete(Integer productId){
        Product newProduct = productRepository.findTopByProductId(productId);
        if (newProduct == null){ throw new NotFoundException("product-id-tidak-ditemukan"); }
        productRepository.delete(newProduct);
    }
}
