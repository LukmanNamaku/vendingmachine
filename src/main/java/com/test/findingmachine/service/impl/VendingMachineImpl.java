package com.test.findingmachine.service.impl;

import com.test.findingmachine.dto.PurchaseDTO;
import com.test.findingmachine.dto.PurchaseRequestDTO;
import com.test.findingmachine.dto.TrxMachineDTO;
import com.test.findingmachine.entity.Product;
import com.test.findingmachine.entity.TrxMachine;
import com.test.findingmachine.handler.NotFoundException;
import com.test.findingmachine.repository.ProductRepository;
import com.test.findingmachine.repository.TrxMachineRepository;
import com.test.findingmachine.service.VendingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.math.BigDecimal;

/**
 * @Project findingmachine
 * @Package com.test.findingmachine.service.impl
 * @Author Lukman Ardi, email : lukman.ardie@gmail.com - PC:ASUS-N56V
 * @Create 25/08/2023
 */

@Service
public class VendingMachineImpl implements VendingService {

    @Autowired
    private TrxMachineRepository trxMachineRepository;

    @Autowired
    private ProductRepository productRepository;

    @Override
    public TrxMachine getTrxById(Integer trxId) {
        TrxMachine newTrxMachine = trxMachineRepository.findTopByTrxId(trxId);
        return newTrxMachine;
    }

    @Override
    public TrxMachine insertCoin(Integer trxId, BigDecimal coin) {
        TrxMachine trxMachine = trxMachineRepository.findTopByTrxId(trxId);
        TrxMachine newTrxMachine = new TrxMachine();
        if(trxId == null || trxMachine == null){
            System.out.println("Set New Transaction");
            trxMachine.setBalance(coin);
            trxMachine.setReturnedCoins(BigDecimal.valueOf(0));
            trxMachine.setProductId(0);
            trxMachineRepository.save(newTrxMachine);
            newTrxMachine = trxMachineRepository.findTopByOrderByTrxIdDesc();

        } else {
             System.out.println("Update Balance");
             BigDecimal updateBalance = trxMachine.getBalance().add(coin);
             trxMachine.setTrxId(trxId);
             trxMachine.setBalance(updateBalance);
             trxMachineRepository.save(trxMachine);
             newTrxMachine = trxMachineRepository.findTopByTrxId(trxId);
        }
        return newTrxMachine;
    }

    @Transactional
    @Override
    public TrxMachine purchaseProduct(PurchaseRequestDTO purchaseRequestDTO) {
        TrxMachine trxMachine = trxMachineRepository.findTopByTrxId(purchaseRequestDTO.getTrxId());
        Product product = productRepository.findTopByProductId(purchaseRequestDTO.getProductId());
        TrxMachine newTrxMachine = new TrxMachine();
        if(trxMachine != null){
            //Update Transaction
            BigDecimal returnedCoins = trxMachine.getBalance().subtract(product.getProductPrice());
            newTrxMachine.setTrxId(purchaseRequestDTO.getTrxId());
            newTrxMachine.setProductId(purchaseRequestDTO.getProductId());
            newTrxMachine.setBalance(returnedCoins);
            newTrxMachine.setReturnedCoins(returnedCoins);
            trxMachineRepository.save(newTrxMachine);

            //Update Quantity
            product.setProductId(purchaseRequestDTO.getProductId());
            product.setProductQty(product.getProductQty() - 1);
            productRepository.save(product);

        }else{
            throw new NotFoundException("data-transaksi-tidak-ditemukan");
        }
        return newTrxMachine;
    }



    @Override
    public PurchaseDTO purchaseResponse(PurchaseRequestDTO purchaseRequestDTO) {
        TrxMachine trxMachine = trxMachineRepository.findTopByTrxId(purchaseRequestDTO.getTrxId());
        Product product = productRepository.findTopByProductId(purchaseRequestDTO.getProductId());
        PurchaseDTO newPurchaseDTO = new PurchaseDTO();
            newPurchaseDTO.setTrxId(trxMachine.getTrxId());
            newPurchaseDTO.setProduct(product);
            newPurchaseDTO.setBalance(trxMachine.getBalance());
            newPurchaseDTO.setReturnedCoins(trxMachine.getReturnedCoins());
        return newPurchaseDTO;
    }

    @Override
    public TrxMachine coinReturn(PurchaseRequestDTO purchaseRequestDTO){
        TrxMachine newTrxMachine = trxMachineRepository.findTopByTrxId(purchaseRequestDTO.getTrxId());
        return newTrxMachine;
    }

    @Override
    public TrxMachineDTO getAccount(TrxMachineDTO accountDTO){
        TrxMachineDTO newAccountDTO = new TrxMachineDTO();
        newAccountDTO.setProductId(accountDTO.getProductId());
        newAccountDTO.setBalance(accountDTO.getBalance());
        newAccountDTO.setReturnedCoins(accountDTO.getReturnedCoins());
        return newAccountDTO;
    }

}
