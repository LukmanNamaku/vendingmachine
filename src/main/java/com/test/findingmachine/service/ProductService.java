package com.test.findingmachine.service;

import com.test.findingmachine.dto.CreateProductDTO;
import com.test.findingmachine.dto.UpdateProductDTO;
import com.test.findingmachine.entity.Product;

import java.util.List;

/**
 * @Project findingmachine
 * @Package com.test.findingmachine.service
 * @Author Lukman Ardi, email : lukman.ardie@gmail.com - PC:ASUS-N56V
 * @Create 25/08/2023
 */

public interface ProductService {

    List<Product>getAllProduct();
    Product getOneProduk(Integer productId);
    void create(CreateProductDTO createProductDTO);
    void update(UpdateProductDTO updateProductDTO);
    void delete(Integer id);

}
