package com.test.findingmachine.service;

import com.test.findingmachine.dto.PurchaseDTO;
import com.test.findingmachine.dto.PurchaseRequestDTO;
import com.test.findingmachine.dto.TrxMachineDTO;
import com.test.findingmachine.entity.TrxMachine;

import java.math.BigDecimal;

/**
 * @Project findingmachine
 * @Package com.test.findingmachine.service.impl
 * @Author Lukman Ardi, email : lukman.ardie@gmail.com - PC:ASUS-N56V
 * @Create 24/08/2023
 */


public interface VendingService {

    TrxMachineDTO getAccount(TrxMachineDTO accountDTO);
    TrxMachine getTrxById(Integer TrxId);
    TrxMachine insertCoin(Integer trxId, BigDecimal coin);
    TrxMachine purchaseProduct(PurchaseRequestDTO purchaseRequestDTO);
    PurchaseDTO purchaseResponse(PurchaseRequestDTO purchaseRequestDTO);
    TrxMachine coinReturn(PurchaseRequestDTO purchaseRequestDTO);
}
